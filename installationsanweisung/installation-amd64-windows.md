# Note
- This test environment was developed on macOS Sonoma 14.4.1. Alternatively, here is a description of the installation using Windows 11 (amd64)

# Requirements
- windows 11 installed (should also work on windows 10)
- 16 GB RAM & 2 CPUS (used here: 4 CPUS)
- 20 GB free storage space
- git installed

# Installation on windows 11
- open powershell as admin
- install wsl for linux kernel, here version 2.1.5.0, kernelversion: 5.15.146.1-2
`wsl --install` 

# download docker desktop
- go to: https://docs.docker.com/desktop/release-notes/
- download installer for version 4.31.0
- double klick on the exe file in your Downloads folder
- be sure wsl is used: go to settings at Docker Desktop (usually on top menu) > Resource > enable "Use the WSL 2 based engine"
- restart docker
- note: docker has to run if you want to use minikube as minikube is running in a docker container
- register for docker hub and login
- docker desktop is now running

# install minikube on windows 11
- go to: https://github.com/kubernetes/minikube/releases/tag/v1.30.0
- download: minikube-installer.exe

* alternativly with powershell
# download minikube binary
Invoke-WebRequest -Uri https://storage.googleapis.com/minikube/releases/1.30.0/minikube-installer.exe -OutFile minikube-installer.exe

# install minikube
Start-Process .\minikube-installer.exe -Wait
* end of powershell alternative

# install kubectl
curl.exe -LO "https://dl.k8s.io/release/v1.30.0/bin/windows/amd64/kubectl.exe"
curl.exe -LO "https://dl.k8s.io/v1.30.0/bin/windows/amd64/kubectl.exe.sha256"
CertUtil -hashfile kubectl.exe SHA256
type kubectl.exe.sha256

- if true checksum is fine and you can continue
- check if correct kubectl version is installed
kubectl version --client
Client Version: v1.30.0

# troubleshouting if minikube or kubectl is not working on command line
- go to system > search for env. variables > make sure minikube and kubectl are set in PATH for system env. variable at PATH
- e. g.: if minikube exe is installed in C:\minikube add this path as new path to env. variables

# add testumgebung.de to your local network
- open Editor as admin
- go to open
- search file: Windows (C:)\System32\drivers\etc
- go to last line
- add:
127.0.0.1       testumgebung.de
::1             testumgebung.de

# start minikube as in README_quick_start-windows.md

