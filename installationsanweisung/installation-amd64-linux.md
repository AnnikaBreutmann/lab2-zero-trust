# install debian 12 with vm
- download debian 12 image
`https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/`

# Configuration of Debian 12 (here using UTM, other VM software likely contains similar configuration options)

- Select a new VM
- Operating System: Linux
- Boot ISO Image Path: to the downloaded Debian 12 amd64 image > Continue
- Select approximately 16 GB (Istio requires at least 16 GB), CPUs: 4, 64 GB, optional: Shared Folder > Save
- Start the VM
- Used here: <br>
    Graphical install > English > United States > American English > Hostname: debian > Domain name: <can be left blank> > root password: debian <or your own> > Full name for user & username account: user > Password: user > Time zone: Eastern > Guided - use entire disk > All files in one partition > Finish partitioning and write changes to disk > yes > Configure the package manager: No > mirror: United States > deb.debian.org > http proxy: <leave blank> > Configuring popularity-contest: No > Software selection checked: GNOME (optional), SSH server, standard system utilities
- Important: Unmount the ISO using UTM, remove the VM CD/DVD Debian 12 Image
- Start the VM

# install minikube
- add user to sudo group
`su root`<br>
`sudo usermod -a -G sudo user`<br>
`exit`<br>
`su - user`<br>
`sudo apt update`<br>
`sudo apt install curl`<br>

## add project folder
`sudo apt install git`<br>
`cd Dokumente`<br>
- load project folder with git `lab1`, `lab2` or `lab2-zero-trust`
- for example `git clone https://gitlab.com/AnnikaBreutmann/lab2-zero-trust.git`<br>
`cd lab2-zero-trust`<br>

- install v1.30.0<br>
`curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.30.0/minikube-linux-amd64`<br>
`sudo chmod +x minikube`<br>
`sudo mv ~/Downloads/minikube-linux-amd64 /usr/local/bin/`<br>

- check version
`minikube version`<br>
- result: v1.30.0

- install kubectl
`curl -LO https://dl.k8s.io/release/v1.30.0/bin/linux/amd64/kubectl`<br>
- validate check sum
`curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl.sha256"`<br>
`echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check`<br>
- install kubectl
`sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl`<br>

## use docker as driver
- deinstall dependecies
`for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done`

# add Docker's official GPG key:
`sudo apt-get update`
`sudo apt-get install ca-certificates curl`
`sudo install -m 0755 -d /etc/apt/keyrings`
`sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc`
`sudo chmod a+r /etc/apt/keyrings/docker.asc`

# add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

- list all docker images
`apt-cache madison docker-ce`
`sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=26.1.4 containerd.io`<br> 

- check if installation was successful 
`docker --version`
- save docker in env
`sudo usermod -aG docker $USER && newgrp docker`

## minikube mit docker starten

- start minikube
`minikube start --driver=docker --kubernetes-version=v1.30.0 --cpus=4 --memory=15976`<br>
- (optional)
`minikube dashboard`<br>

**Follow README.md or README-quickstart.md**

# install istio
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.21.0 TARGET_ARCH=amd64 sh -
export PATH="$PATH:/home/user/lab/zero-trust/istio/istio-1.21.0/bin
istioctl install --set profile=demo -y
kubectl label namespace default istio-injection=enabled
kubectl delete pods --all -n default
kubectl get pods

istioctl analyze
kubectl get ns
kubectl get pods -n istio-system
kubectl exec "$(kubectl get pod -l app=webserver -o jsonpath='{.items[0].metadata.name}')" -c webserver -- curl -sS localhost:8001/ | grep -o "<title>.*</title>"




