# Versionierung
- macOS Sonoma 14.4.1*
- Docker Desktop: 4.30.0
- Docker Client: 26.1.1*
- Kubernetes:
    - Minikube: v1.30.0* 
    - Kubernetes: v1.30.0*
- Firefox: 126 (64-Bit)* 
- Chrome: Version 122.0.6261.94*
- Safari: Version 17.4.1*
- Istio: Version 1.21.0 (osx-arm64)*
- OPA: v.0.64.1
- PostgreSQL: postgres:16.3
- pgAdmin: dpage/pgadmin4:8.6
- Node.js: Version v21.7.3*
* arm64 für Apple Silicion M2

**Hinweis: Diese Arbeit wurde auf einem macOS, M2, arm64, Sonoma 14.4.1 programmiert und getestet. Zur Nachvollziehbarkeit wurde die Testumgebung auch auf einer amd64 Architektur auf Windows 11 installiert. Aus diesem Grund befinden sich in diesem Installationsordner zwei Installationsanweisungen.**

## Minikube: Installation auf macOs, Sonoma 14.4.1, Apple Silicon M2
- siehe /installationsanweisung/installation-macOs.md

## Minikube: Installation auf Windows 11
- siehe /installationsanweisung/installation-amd64-windows.md

**Nach der Installationsanweisung**
# Bereitstellen der Testumgebung in Minikube
## Minikube starten
- Istio gibt eine erforderliche Speicherkapazität von 16 GB an. Hier wurden folgende Angaben ebenso erfolgreich verwendet, auch gar keine Angaben zu Memory waren problemlos durchführbar:<br>
`minikube start --driver=docker --kubernetes-version=v1.30.0 --cpus=4 --memory=15976`<br>

### Check status Minikube
`minikube status`<br>
Ergebnis sollte dies anzeigen:<br>
- minikube
- type: Control Plane
- host: Running
- kubelet: Running
- apiserver: Running
- kubeconfig: Configured

## 1. Alle persistenten Speicher laden (aus dem Projektordner heraus)
`kubectl apply -f ./K8s/pv-volume.yaml`<br>
`kubectl apply -f ./K8s/pv-claim.yaml`<br>
`kubectl apply -f ./K8s/volume-public.yaml` //zum Speichern der Fotos im Minikube

## 2. Alle Secrets
`kubectl apply -f ./backend/postgres/postgres-secret.yaml`<br>
`kubectl apply -f ./backend/postgres/pgadmin-secret.yaml` // optional: pgadmin4 als Pod

## 3. Service Accounts
`kubectl apply -f ./K8s/service-accounts.yaml`

## 4. Deployments (Services sind in den yaml-Dateien bereits enthalten)
`kubectl apply -f ./backend/postgres/postgres-deployment.yaml`<br>
`kubectl apply -f ./backend/postgres/pgadmin-deployment.yaml` // optional, falls pgadmin erwünscht<br>

## 5. Postgres Datenbank und Tabellen erstellen
- s. README_postgres.md

## 6. Deployments Anwendungen
`kubectl apply -f ./K8s/fotomanager-deployment.yaml`<br>
`kubectl apply -f ./K8s/idp-deployment.yaml`<br>
`kubectl apply -f ./K8s/webserver-deployment.yaml`<br>

**Hinweis:**
- Die Anwendung ist nur erreichbar, wenn das Istio-Gateway installiert wurde, daher ist zum Erreichen der Anwendung die Istio-Installation erforderlich

# Zero Trust
## 1. Istio
## Installation
**Installationsanweisung für macOS (Installation von Istio ist für Linux abweichend, s. weiter unten)**
`cd zero-trust/istio/istio-1.21.0`<br>
`export PATH=$PWD/bin:$PATH`<br>
- oder dauerhaftes Hinzufügen von istioctl zur shell (hier zsh)
`export PATH=/path/to/istio/bin:$PATH`<br>
**Installation für Debian 12, arm64:**
`cd zero-trust/istio/istio-1.21.0-linux`<br>
`export PATH=$PWD/bin:$PATH`<br>
- oder dauerhaftes Hinzufügen von istioctl zur shell (hier zsh)
`export PATH=/path/to/istio/bin:$PATH`<br>

- Installation von Istio in Minikube<br>
`istioctl install --set profile=demo -y`<br>
(Enthält Beispiele zum Testen sowie Monitoring Tools)
- Labeln, damit Istio die Pods zuordnen kann<br>
`kubectl label namespace default istio-injection=enabled`<br>
- Pod neu wiederherstellen<br>
`kubectl delete pods --all -n default`<br>
- Test: nun sollten 2 Container pro Deployment laufen (bei Replicas: 1)<br>
`kubectl get pods`<br>

### Checks
Zur Analyse von istio<br>
`istioctl analyze`<br>
Ausgabe sollte ungefähr so aussehen:<br>
✔ No validation issues found when analyzing namespace: default.<br>

## istio gateway und visualServices erstellen
Secret erstellen für istio-gateway (aus dem Ordner testumgebung-certs)<br>
`cd K8s/testumgebung-certs`<br>
`kubectl create secret -n istio-system tls istio-gateway-certs --cert=istio-gateway.crt --key=istio-gateway.key`<br>
Zurückkehren zum Projektordner
`cd ../..`<br>

Deplyoment von istio-gateway und virtualService
`kubectl apply -f ./K8s/istio-gateway.yaml`<br>
`kubectl apply -f ./K8s/istio-VS-gateway.yaml`<br>
<br>

ggf. Istio Gateway neu starten<br>
`kubectl rollout restart deployment istio-ingressgateway -n istio-system`<br>
Weitere Tests<br>
`kubectl logs <istio-pod> -n istio-system`<br>

# Anwendung starten
## DNS Eintrag für macOS ändern (Anleitung zu Debian in der Datei installation-vm.md im Ordner installationsanweisung)
DNS-Eintrag erstellen (bei macOS Sonoma): 
`127.0.0.1      testumgebung.de`<br>
`::1            testumgebung.de`<br>
`:wq`<br>

*Hinweis: Da die Anwendung lediglich über ein selbstsigniertes Zertifikat verfügt, ist es erforderlich, über die erweiterten Einstellungen die Anwendung aufzurufen.*

## Anwendung starten
- alle Pods müssen laufen, check:
`kubectl get pods`<br>
- Tunnel öffnen
`minikube tunnel`<br>
- Anwendung aufrufen
- `curl https://testumgebung.de -k`
- oder über den Browser https://testumgebung.de

## Rolle als root admin anlegen
### User und Admins anlegen
- über Login / Registrierung User oder Admins anlegen
- root-admin: dem ersten Admin muss direkt in der Datenbank die Rolle admin zugewiesen werden:

### Direkt aus dem postgres pod
`kubectl get pods`<br>
`kubectl exec -it <pod postgres> -- /bin/bash`<br>
`psql -U postgres`<br>
`\c user_db;`<br>
`\dt`<br>
`UPDATE userdata SET role = 'admin' WHERE username = 'admin';`<br>
`exit`<br>
`exit`<br>

### Alternativ mittels pgadmin
`kubectl port-forward service/pgadmin-service 8000:80`<br>
`http://localhost:8000:80`<br>
Anmeldung<br>
- username: admin@admin.de // entsprechend dem Secret
- password: admin13 // entsprechend dem Secret

    - Server > Register
    - Connection:
        - host: postgres-service
        - port: 5432
        - maintenance database: postgres
        - username: posstgres // entsprechend dem Secret
        - Password: posstgres13 // entsprechend dem Secret
> user_db auswählen > Schemas > Tables > userdata > (Rechtsklick) View/Edit Data > role ändern von user zu admin<br>
Speichern<br>

<div>
Nun kommt man über den Login auch in den Admin-Bereich
- Admins können weitere Admins hinzufügen
     
## Bilder als user hochladen
- Registrierung als user
- Login als user
- Bidler aus Ordner hochladen /Bilder-fuer-upload
     
## Kiali installieren
`cd zero-trust/istio/istio-1.21.0`<br>
`kubectl apply -f samples/addons`<br>
`kubectl rollout status deployment/kiali -n istio-system`<br>
`istioctl dashboard kiali`<br>

# 2. Opa
## Installation von Opa

Opa-Envoy installieren<br>
`kubectl apply -f ./K8s/opa-deployment.yaml`<br>
`kubectl apply -f opa-serviceaccount.yaml`
`kubectl rollout restart deployment admission-controller -n opa-istio`

## Istio Mesh konfigurieren
`kubectl edit configmap -n istio-system istio`<br>

Folgende Zeilen hinzufügen:<br>
- unter extensionProviders:
Änderungsmodus:<br>
`i`<br>
```
    - name: opa-ext-authz-grpc
      envoyExtAuthzGrpc:
        service: opa-ext-authz-grpc.local
        port: 9191
        includeRequestBodyInCheck:
          maxRequestBytes: 2097152000
          packAsBytes: false
```
Beenden:<br>
`:wq`

## OPA-Sidecars hinzufügen
`kubectl label namespace default opa-istio-injection="enabled"`

## AuthorizationPolicy anpassen
`kubectl apply -f ./K8s/istio-AP-opa.yaml`<br>
`kubectl apply -f ./K8s/opa-istio-configmap.yaml`<br>

Über das Löschen aller Pods im namespace default (muss nicht angegeben werden), werden die OPA Container hinzugefügt
`kubectl delete pods --all`<br>

#### Check
`kubectl get pods`<br>
NAME                          READY   STATUS    RESTARTS   AGE<br>
fotomanager-8956f85b6-5xztz   3/3     Running   0          20s<br>
idp-78c7f6c88-98kld           3/3     Running   0          20s<br>
pgadmin-587b7895b-6xdcl       3/3     Running   0          20s<br>
postgres-684f4945-jzgqn       3/3     Running   0          20s<br>
webserver-7fc9db65b9-j6rfm    3/3     Running   0          20s<br>
`kubectl get gateway`<br> 
NAME                    AGE<br>
istio-gateway   14m<br>

`kubectl get authorizationpolicy`<br>
NAME        AGE<br>
ext-authz   12m<br>
webserver   99s<br>

## mTLS installieren im gesamten Cluster
`kubectl label namespace opa-istio istio-injection="enabled"`<br>
`kubectl delete pods --all -n -opa-istio`<br>
`kubectl get pods -n opa-istio`<br>
- jetzt müssen 2/2 jeweils 2 Container pro Pod laufen
`kubectl apply -f ./K8s/istio-mtls-default.yaml`<br>
`kubectl apply -f ./K8s/istio-DR-mTLS-mesh.yaml`<br>
`kubectl rollout restart deployment istio-ingressgateway -n istio-system`<br>
`kubectl delete pods --all -n opa-istio`<br>
`kubectl delete pods --all`<br>

# Die Anwendung kann nun mit den Zero-Trust-Komponenten getestet werden
s. Datei: Hinweise-Trust-Level.md

# Uninstall
## OPA deinstallieren
- Aus dem Projektordner/K8s
`kubectl delete -f opa-deployment.yaml`
`kubectl delete authorizationpolicy webserver`

## Istio deinstallieren
`istioctl uninstall -y --purge`
`kubectl delete namespace istio-system`
`kubectl label namespace default istio-injection-`
`kubectl delete gateway istio-system/istio-ingressgateway`

Alle VirutalServices und DestinationRules entfernen
`kubectl delete virtualservice <name>`
`kubectl delete destinationrule <name>`

Sidecar-Pods entfernen
- Pods aktualisieren und die Sidecar-Container aus den spec.containern entfernen

# Hinweise
## zu Istio
Überprüfen, dass kein Prozess auf UID 1337 läuft, da dies für den Istio-Sidecar-Proxy reserviert ist:
`kubectl get pods <name-of-pod> -o yaml`
Labels, Versionen und service ports sollten explizit benamt sein, s. das folgende Beispiel.

```
apiVersion: v1
    kind: Pod
    metadata:
        name: mypod
        labels:
            app: myapp
            version: v1
    spec:
    containers:
    - name: mycontainer
        image: myimage
```


Beispiel für Service:

    ```
    apiVersion: v1
        kind: Service
        metadata:
        name: myservice
        spec:
        selector:
            app: myapp
        ports:
        - name: http
            protocol: TCP
            port: 80
            targetPort: 8080
        - name: grpc
            protocol: TCP
            port: 50051
            targetPort: 50051
    ```
# Ändern der Images
## Docker: Images erstellen und zu Docker Hub hochladen
Aus den jeweiligen Ordnern zu idp, fotomanager und webserver heraus
### (falls erforderlich) docker image erstellen von idp, fotomanager, webserver
`docker login`<br>
`docker build <reponame>/idp:<version> .`<br>
`docker push <reponame>/idp:<version>`<br>
`docker build <reponame>/fotomanager:<version> .`<br>
`docker push <reponame>/fotomanager:<version>`<br>
`docker build <reponame>/webserver:<version> .`<br>
`docker push <reponame>/webserver:<version>`<br>
