# Installationsanweisung für macOs, Apple Silicon, M2, Somoma 14.4.1
Download Docker Desktop von: https://docs.docker.com/desktop/release-notes
- Version 4.30.0 auswählen
- .dmg File herunterladen und durch Doppelklick den Installationsassistenten starten
- Docker kann dann als Driver für Minikube verwendet werden und über die Auswahl der gewünschten Imageversion wird das Minikube direkt von Docker geladen
- Docker als Driver verwenden
`minikube config set driver docker`<br>
`minikube start --driver=docker`<br>





