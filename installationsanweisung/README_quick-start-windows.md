**After the installation of Minikube, kubectl and Docker, this faster variant can also be selected if there are no capacity problems.**

# start minikube & wait until finished, this could take some time
`minikube start --driver=docker --kubernetes-version=v1.30.0 --cpus=4 --memory=15976`<br>
*Note: It is also possible to use less storage space. It was possible to run the application without any problems even without explicitly specifying memory.*

### check minikube status
`minikube status`<br>
Should be like:
- minikube
- type: Control Plane
- host: Running
- kubelet: Running
- apiserver: Running
- kubeconfig: Configured

# cd ~/lab2-zero-trust load application with persistent volumes, service accounts, applications
`kubectl apply -f ./K8s/testumgebung-quick-start-windows.yaml`<br>

# check if pods are running
`kubectl get pods`<br>
- like this (pod names could change after -)
- alle container should be running (1/1)
NAME                           READY   STATUS    RESTARTS   AGE
fotomanager-699cbbc6bf-8wl45   1/1     Running   0          6m56s
idp-579b7c455-kmmmb            1/1     Running   0          6m56s
pgadmin-574db4bb44-559h6       1/1     Running   0          6m56s
postgres-5b7f46c847-vvzmv      1/1     Running   0          6m56s
webserver-848869d4c7-dzlxl     1/1     Running   0          6m56s

# create database
`kubectl get pods`<br>
`kubectl exec -it <copy pod-full-name> -- /bin/bash`<br>
`root@postgres-0:/# createdb -U postgres user_db`<br>
`psql -U postgres -d user_db`<br>

# create table userdata
-- Create table userdata
-- Create the sequence first
CREATE SEQUENCE userdata_id_seq;

-- Create the table
CREATE TABLE IF NOT EXISTS public.userdata
(
    username character varying(200) COLLATE pg_catalog."default",
    name character varying(200) COLLATE pg_catalog."default",
    surname character varying(200) COLLATE pg_catalog."default",
    email character varying(200) COLLATE pg_catalog."default",
    password character varying(1000) COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('userdata_id_seq'::regclass),
    role character varying(50) COLLATE pg_catalog."default" DEFAULT 'USER',
    CONSTRAINT userdata_pkey PRIMARY KEY (id),
    CONSTRAINT username UNIQUE (username)
)
TABLESPACE pg_default;

# create table sessions
-- Table: public.sessions

-- DROP TABLE IF EXISTS public.sessions;

CREATE TABLE IF NOT EXISTS public.sessions
(
    sid character varying COLLATE pg_catalog."default" NOT NULL,
    sess json NOT NULL,
    expire timestamp(6) without time zone NOT NULL,
    user_id integer,
    CONSTRAINT sessions_pkey PRIMARY KEY (sid),
    CONSTRAINT fk_user_sessions FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT sessions_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

-- Index: idx_sessions_expire

-- DROP INDEX IF EXISTS public.idx_sessions_expire;

CREATE INDEX IF NOT EXISTS idx_sessions_expire
    ON public.sessions USING btree
    (expire ASC NULLS LAST)
    TABLESPACE pg_default;

# create tabel fotos
-- Table: public.fotos

-- DROP TABLE IF EXISTS public.fotos;

-- Erstelle die Sequenz
CREATE SEQUENCE fotos_id_seq;

-- Erstelle die Tabelle
CREATE TABLE IF NOT EXISTS public.fotos
(
    id integer NOT NULL DEFAULT nextval('fotos_id_seq'::regclass),
    image_path text COLLATE pg_catalog."default",
    title character varying(255) COLLATE pg_catalog."default",
    user_id integer,
    CONSTRAINT fotos_pkey PRIMARY KEY (id),
    CONSTRAINT fk_userdata FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);

- check if all 3 tables are installed
`\dt`

# exit pod
`exit`<br>
`exit`<br>

# install istio and istio gateway to access the web application
- install istio and use istioctl on command line
`cd zero-trust/istio-1.21.0`<br>
`pwd`
- copy path to:
- go to system > type in search env variables. Add path to system variables with
- new
- name of variable: istio value of variable: <copy path>
- save
- check if istioctl is available at `istioctl`

`istioctl install --set profile=demo -y`<br>
´

- restart pods<br>
`kubectl delete pods --all -n default`<br>
`kubectl get pods`<br>
- now there should be 2 container running in one pod<br>

# check if istio is working well
`istioctl analyze`<br>
- should be:<br>
✔ No validation issues found when analyzing namespace: default.<br>

# install istio gateway and virtualServics
- create secret and certiificate for the gateway to enable https
`cd K8s/testumgebung-certs`<br>
`kubectl create secret -n istio-system tls istio-gateway-certs --cert=istio-gateway.crt --key=istio-gateway.key`<br>
`cd ../..`<br>
`kubectl apply -f ./K8s/istio-gateway.yaml`<br>
`kubectl apply -f ./K8s/istio-VS-gateway.yaml`<br>

# access https://testumgebung.de via browser
## change DNS
- open Editor as admin
- go to > Open file > windows/system32/etc/hosts
- go to last line and add:
`127.0.0.1      testumgebung.de`<br>
`::1            testumgebung.de`<br>
- save file

## start application
- check if all pods are running
`kubectl get pods`<br>
`minikube tunnel`<br>
- open application in browser: https://testumgebung.de

*Note: As the application only has a self-signed certificate for test reasons, it is necessary to access the application via the advanced settings.*

## Create role as root admin
### Create users and admins
- Create users or admins via login / registration
- root-admin: the first admin must be assigned the admin role directly in the database:

### Directly from the postgres pod
`kubectl get pods`<br>
`kubectl exec -it <pod postgres> -- /bin/bash`<br>
`psql -U postgres`<br>
`\c user_db;`<br>
`\dt`<br>
`UPDATE userdata SET role = 'admin' WHERE username = 'admin';`<br>
`exit`<br>
`exit`<br>

<div>
Now you can also access the admin area via the login
- Admins can add further admins
     
## Upload images as user
- Register as user
- Login as user
- Upload images from folder /images-for-upload
     
## Install Kiali
`cd zero-trust/istio/istio-1.21.0`<br>
`kubectl apply -f samples/addons`<br>
`kubectl rollout status deployment/kiali -n istio-system`<br>
`istioctl dashboard kiali`<br>

# 2. Opa
## Installation from Opa
- install Opa-Envoy
`kubectl apply -f ./K8s/opa-deployment-windows.yaml`<br>
`kubectl apply -f ./K8s/opa-serviceaccount.yaml`<br>
`kubectl rollout restart deployment/admission-controller -n opa-istio`<br>
`kubectl get pods -n opa-istio`<br>
- wait till pod is running

## configure Istio Mesh 
`kubectl edit configmap -n istio-system istio`<br>

- add this lines 
- under extensionProviders:
- go to change mode<br>
`i`<br>
```
    - name: opa-ext-authz-grpc
      envoyExtAuthzGrpc:
        service: opa-ext-authz-grpc.local
        port: 9191
        includeRequestBodyInCheck:
          maxRequestBytes: 2097152000
          packAsBytes: false
```
-  to end<br>
`:wq`

## OPA-Sidecars hinzufügen
`kubectl label namespace default opa-istio-injection=enabled`

## AuthorizationPolicy anpassen
`kubectl apply -f ./K8s/istio-AP-opa.yaml`<br>
`kubectl apply -f ./K8s/opa-istio-configmap.yaml`<br>

By deleting all pods in the namespace default (does not have to be specified), the OPA containers are added
`kubectl delete pods --all`<br>

#### Check
`kubectl get pods`<br>
NAME                          READY   STATUS    RESTARTS   AGE<br>
fotomanager-8956f85b6-5xztz   3/3     Running   0          20s<br>
idp-78c7f6c88-98kld           3/3     Running   0          20s<br>
pgadmin-587b7895b-6xdcl       3/3     Running   0          20s<br>
postgres-684f4945-jzgqn       3/3     Running   0          20s<br>
webserver-7fc9db65b9-j6rfm    3/3     Running   0          20s<br>
`kubectl get gateway`<br> 
NAME                    AGE<br>
istio-gateway   14m<br>

`kubectl get authorizationpolicy`<br>
NAME        AGE<br>
ext-authz   12m<br>
webserver   99s<br>

## mTLS installieren im gesamten Cluster
`kubectl label namespace opa-istio istio-injection="enabled"`<br>
`kubectl delete pods --all -n -opa-istio`<br>
`kubectl get pods -n opa-istio`<br>
- wait till 2 container are running 
`kubectl apply -f ./K8s/istio-mtls-default.yaml`<br>
`kubectl apply -f ./K8s/istio-DR-mTLS-mesh.yaml`<br>
`kubectl rollout restart deployment istio-ingressgateway -n istio-system`<br>
`kubectl delete pods --all -n opa-istio`<br>
`kubectl delete pods --all`<br>

# The application can now be tested with the Zero Trust components
`minikube tunnel`
- go to: https://testumgebung.de
see file: Hinweise-Trust-Level.md

# Uninstall
## OPA deinstallieren
- ~./lab2-zero-trust/K8s
`kubectl delete -f opa-deployment-windows.yaml`
`kubectl delete authorizationpolicy webserver`

## Istio deinstallieren
`istioctl uninstall -y --purge`
`kubectl delete namespace istio-system`
`kubectl label namespace default istio-injection-`
`kubectl delete gateway istio-system/istio-ingressgateway`

Remove all VirutalServices and DestinationRules
`kubectl delete virtualservice <name>`
`kubectl delete destinationrule <name>`

Remove sidecar pods
- Update pods and remove the sidecar containers from the spec.containers
