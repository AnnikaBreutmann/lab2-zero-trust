# useful commands for using minikube
minikube status # shows if minikube is working fine
minikube dashboard # opens gui for minikub, sometimes very helpful
kubectl get pods # shows all pods in the default namespace
kubectl get pods -n istio-system # shows all pods in the istio namespace
kubectl get pods -n opa-istio # shows all pods in the opa namespace
kubectl logs <full-name-of-pod> # shows all logs concerning the whole pod
kubectl logs <full-name-of-pod> -c opa # shows all logs of the opa container in the named pod (all default ns pods are opa injected and so an opa container is running in each)
kubectl set image deployment/idp idp=breutmannannika/idp:zt-amd-1 # changes the image in the pod to the taged idp image zt-amd-1. You have to delete the pod that it gets restarted with the new image
kubectl delete pods --all # all pods in default namespace are deleted
kubectl delete pods --all -n opa-istio # all pods of namespace opa-istio are deleted

# if testumgebung.de is not running
- make sure 127.0.0.1 testumgebung.de is set in hosts file
- delete browser, browser cookies and settings
- restart minikube tunnel with
minikube tunnel --cleanup

# if webserver pod is not running after installing opa
- try again to load pods, sometimes it takes some time
kubectl delete pods --all
- make sure istio configmap is configured correctly
`kubectl edit configmap -n istio-system istio`<br>

Folgende Zeilen hinzufügen:<br>
- add the following lines:
`i`<br>
```
    - name: opa-ext-authz-grpc
      envoyExtAuthzGrpc:
        service: opa-ext-authz-grpc.local
        port: 9191
        includeRequestBodyInCheck:
          maxRequestBytes: 2097152000
          packAsBytes: false
```
- esc
`:wq`