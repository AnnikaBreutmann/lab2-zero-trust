const passport = require('passport');

const logoutStrategy = new passport.Strategy((req, done) => {
  // Beenden Sie hier die Sitzung (z.B. mit Express-Session)
  req.session.destroy((err) => {
    if (err) {
      console.error('Fehler beim Beenden der Sitzung:', err);
      return done(err);
    }
    
    // Erfolgreicher Logout
    done(null); // Keine zusätzlichen Argumente für erfolgreiches Logout
  });
});

module.exports = logoutStrategy;
