
const { compareSync } = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;
const passport = require('passport');

const { Pool } = require('pg');
const userConfig = require('./configs/configDB');
const pool = new Pool(userConfig);

passport.use(new LocalStrategy(
    {
      usernameField: 'usernameOrEmail',
      passwordField: 'password',
    },
    async (usernameOrEmail, password, done) => {
      try {
        console.log('Authentifizierung gestartet:', { usernameOrEmail, password });
  
        const result = await pool.query('SELECT * FROM userdata WHERE username = $1 OR email = $1', [usernameOrEmail]);
  
        if (result.rows.length === 0) {
          console.log('Benutzer nicht gefunden');
          return done(null, false, { message: 'Passwort oder E-Mail nicht korrekt.' });
        }
  
        const user = result.rows[0];
  
        const isMatch = await bcrypt.compare(password, user.password);
  
        if (!isMatch) {
          console.log('Passwort stimmt nicht überein');
          return done(null, false, { message: 'Falsches Passwort.' });
        }
  
        console.log('Authentifizierung erfolgreich:', user.username);
        const userDataInSession = {
          id: user.id,
          username: user.username,
          role: user.role, 
        };
  
        return done(null, userDataInSession);
      } catch (err) {
        console.error('Fehler bei der Authentifizierung:', err);
        return done(err);
      }
    }
  ));

  // Serialisierung und Deserialisierung des Benutzers
passport.serializeUser(function (user, cb) {
    process.nextTick(function () {
      console.log('Serialized User aus der Funktion:', user);
      cb(null, { id: user.id, username: user.username, role: user.role });
    });
  });
  
  passport.deserializeUser(function (user, cb) {
    console.log('Deserialized User aus der Funktion:', user);
    process.nextTick(function () {
    cb(null, user);
    });
  });
//Persists user data inside session
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

//Fetches session details using session id
passport.deserializeUser(function (id, done) {
    UserModel.findById(id, function (err, user) {
        done(err, user);
    });
});