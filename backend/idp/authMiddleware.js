// middleware function to ensure authentication
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    } else {
      res.status(401).json({ success: false, message: 'Benutzer nicht authentifiziert' });
    }
}

// middleware function to ensure admin privileges
function ensureAdmin(req, res, next) {
    console.log('ensureAdmin');
    if (req.isAuthenticated() && req.user.role === 'admin') {
      return next();
    }
    res.status(403).json({ success: false, message: 'Benutzer ist kein Admin' });
}

// export middleware
module.exports = { 
    ensureAuthenticated,
    ensureAdmin,
};
