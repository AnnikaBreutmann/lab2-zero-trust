const express = require('express');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;
const session = require('express-session');
const PgSession = require('connect-pg-simple')(session);
const bcrypt = require('bcrypt');
const { Pool } = require('pg');
const userConfig = require('./configs/configDB');
const pool = new Pool(userConfig);
const path = require('path');

// initalize idp express server
const idp = express();

idp.set('view engine', 'ejs');
idp.set('views', path.join(__dirname, 'views'));

// initialize express to user json
idp.use(express.json());  // parse JSON requests first
idp.use(express.urlencoded({ extended: true })); // Parse URL-encoded data with complex structures

// initialize session
idp.use(
  session({
    store: new PgSession({
      pool,
      tableName: 'sessions', 
    }),
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: false,
  })
);

// configure specifical local passport strategy
passport.use(new LocalStrategy(
  {
    // saves only usernamOrEmail and passwort
    usernameField: 'usernameOrEmail',
    passwordField: 'password',
  },
    // start authentication
  async (usernameOrEmail, password, done) => {
    try {
      console.log('Authentifizierung gestartet:', { usernameOrEmail, password });

      const result = await pool.query('SELECT * FROM userdata WHERE username = $1 OR email = $1', [usernameOrEmail]);

      if (!result.rows.length) {
        console.log('Benutzer nicht gefunden');
        return done(null, false, { message: 'Passwort oder E-Mail nicht korrekt.' });
      }

      const user = result.rows[0]; 

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        console.log('Passwort stimmt nicht überein');
        return done(null, false, { success: false, error: 'Fehler bei der Authentifizierung.' });
      }

      console.log('Authentifizierung erfolgreich:', user.username);
      return done(null, user); 
    } catch (err) {
      console.error('Fehler bei der Authentifizierung:', err);
      return done(err);
    }
  }
));

// serialize user
passport.serializeUser((user, done) => {
  done(null, { id: user.id, role: user.role, username: user.username });
});

// deserialize user
passport.deserializeUser((id, done) => {
  pool.query('SELECT * FROM users WHERE id = $1', [id], (err, result) => {
    if (err) {
      return done(err);
    }

    done(null, result.rows[0]);
  });
});

// initialize Passport for authentication
idp.use(passport.initialize());
// use Passport's session middleware for persistent login sessions
idp.use(passport.session());

//---REGISTRATION AND AUTHENTIFICATION---

// initialize 2fa-authentification
let temporaryTOTP = null; 
let generatedTOTP = null;

// function to create secret for 2-fatoken
function formatSecretCode(secret) {
  const formattedCode = Math.floor(Math.random() * 1000000).toString().padStart(6, '0'); // Zufällige 6-stellige Zahl von 0 bis 999999
  return formattedCode; 
}

// rout that generates and prints out 2fa token
idp.get('/auth/2fa/simple', (req, res) => {
  try {
      // set token if not already generated
      if (!generatedTOTP) {
        generatedTOTP = formatSecretCode();
        temporaryTOTP = generatedTOTP;
      } 

      // return successfully generatet token secret code
      res.json({ temporaryTOTP });
  } catch (error) {
      console.error('Fehler beim Generieren des TOTP-Geheimcodes:', error);
      res.status(500).json({ error: 'Interner Serverfehler' });
  }
});

// route for handling 2FA login authentication
idp.post('/auth/login/2fa', passport.authenticate('local'), (req, res) => {
  try {
    const { totpCode } = req.body;

    // Verify token
    if (String(totpCode) !== String(temporaryTOTP)) {
      return res.status(401).json({ success: false, error: "Falsches TOTP-Token." });
    } else {
      // checks if user is authenticated
      if (req.user) {
        const user_id = req.user.id;
        const user = req.user;
        req.session.user = user;
        const username = req.user.username; // extract username directly from current session
        const role = req.user.role; // extract role directly from current session

        // generates jwt token payload
        const tokenPayload = {
          userId: user_id,
          role: role,
          is2FAEnabled: true // sets is2FAEnabled to true 
        }

        globalUserId = user_id;
        globalUserName = username;

        const token = jwt.sign(tokenPayload, 'your_secret_key'); // creates jwt token

        if (role === 'admin' || role === 'user') {
          // sends JWT-token to webserver
          return res.json({ success: true, token, user });
        } else {
          return res.json({ success: false, role: null });
        }
      } else {
        // error handler sends status back to webserverf if token is wrong or authentification went wrong
        return res.status(401).json({ success: false, error: "Falsche Passworteingabe oder Benutzer nicht authentifiziert." });
      }
    }
  } catch (error) {
    // error handler sends status back to webserver
    console.error('Fehler beim Verarbeiten des 2FA-Login-Requests:', error);
    return res.status(500).json({ success: false, error: 'Interner Serverfehler beim 2FA-Login' });
  }
});

let globalUserId;
let globalUserName;

// route for handling regular login authentication
idp.post('/auth/login', passport.authenticate('local'), (req, res) => {
  // check if user is authenticated and verify role and user object
  if (req.user) {
    const user_id = req.user.id;
    const user = req.user;
    req.session.user = user;
    //const user_id = req.session.passport.user.id;
    console.log('So speichert passport Daten', req.session.passport.user);
    console.log('So speichert session Daten', req.session);
    const username = req.session.passport.user.username;
    const role = req.session.passport.user.role;

    // generates jwt token payload
    const tokenPayload = {
      userId: user_id,
      role: role,
      is2FAEnabled: false
    }

    console.log('Benutzerdaten im JWT-Token:', tokenPayload);

    globalUserId = user_id;
    globalUserName = username;

    const token = jwt.sign(tokenPayload, 'your_secret_key');
    console.log('Erstelltes JWT:', token);

    if (role === 'admin') {
      // send back to webserver user has role admin
      return res.json({ success: true, token, user });
    } else if (role === 'user') {
      // send back to webserver user has role user
      return res.json({ success: true, token, user });
    } else {
      // send back false if no role found
      return res.json({ success: false, role: null });
    }
  } else {
    // error handler if req.session has not been defined correctly
    if (req.authInfo && req.authInfo.error) {
      return res.status(401).json({ success: false, error: "Falsche Passworteingabe." });
    } else {
      return res.status(401).json({ success: false, error: "Falsche Passworteingabe." });
    }
  }
});

// route for user registration
idp.post('/auth/register', async (req, res) => {
  const { usernameRegister, nameLogIn, surnameLogIn, email, passwordRegister } = req.body;

  // check if username or email already exists
  const checkUserQuery = 'SELECT username, email FROM userdata WHERE username = $1 OR email = $2';
  const checkUserResult = await pool.query(checkUserQuery, [usernameRegister, email]);

  if (checkUserResult.rows.length > 0) {
    // a user with the username or email already exists
    const existingUser = checkUserResult.rows[0];
    let errorMessage;

    if (existingUser.username === usernameRegister) {
      errorMessage = "Benutzername bereits vergeben.";
      errorColor = "warning"; 
    } else {
      errorMessage = "E-Mail-Adresse existiert bereits.";
      errorColor = "warning"; 
    }

    return res.json({ success: false, error: "Benutzername oder E-Mail-Adresse sind bereits vergeben." });
  }

  const hashedPassword = await bcrypt.hash(passwordRegister, 10);

  const registerQuery = 'INSERT INTO userdata (username, name, surname, email, password, role) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *';

  try {

    const result = await pool.query(registerQuery, [usernameRegister, nameLogIn, surnameLogIn, email, hashedPassword, 'user']);
    return res.status(201).json({ success: true });
  } catch (error) {
    return res.status(500).json({ success: false, error: "Internal Server Error /auth/register" });
  }
});

// route for user logout
idp.post('/auth/logout', function(req, res, next){
  req.logout(function(error) {
    if (error) {
      console.error('Fehler beim Session-Zerstören:', error);
      return next(error);
    }
    console.log('Benutzer ausgeloggt', req.user);
    // check if session has been deleted successfully 
    console.log('Aktuelle Sitzung nach dem Ausloggen:', req.session);
    res.status(200).send('Logout erfolgreich'); 
  });
});

// route for changing user data
idp.post('/auth/changeUserData', async (req, res) => {
  const { username, newPassword } = req.body;
  const userId = globalUserId;

  try {
    let updateQuery = '';
    let updatedFields = [];
    const values = []; // define array for params

    // prepare sql statement for update
    if (username && username.length > 0) {
      updatedFields.push('username = $1');
      values.push(username); // add username as param
    }

    if (newPassword && newPassword.length > 0) {
      const hashedPassword = await bcrypt.hash(newPassword, 10);
      updatedFields.push('password = $2');
      values.push(hashedPassword); // add hashed password as param
    }
    
    if (updatedFields.length === 0) {
      // no changes made, send error message
      return res.status(400).json({ success: false, message: 'Keine Daten zum Ändern eingegeben.' });
    }

    updateQuery = `UPDATE userdata SET ${updatedFields.join(', ')} WHERE id = $${updatedFields.length + 1}`;
    values.push(userId); // add userId as the last param

    // execute the update query
    const updateResult = await pool.query(updateQuery, values);

    // check if new username already exists
    if (username && username.length > 0) {
      const usernameExists = await pool.query('SELECT id FROM userdata WHERE username = $1 AND id != $2', [username, userId]);

      if (usernameExists.rows.length > 0) {
        // send error if username already exists
        return res.status(400).json({ success: false, message: 'Benutzername existiert bereits.' });
      }
    }

    if (updateResult.affectedRows === 0) {
      // send error if update is not made
      return res.status(401).json({ success: false, message: 'Daten konnten nicht geändert werden.' });
    }

    // send success if update has been made
    return res.status(200).json({ success: true, message: 'Benutzername und / oder Passwort erfolgreich geändert.' });
  } catch (error) {
    console.error('Fehler beim Ändern der Benutzerdaten:', error);
    return res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});

// route for changing admin data
idp.post('/auth/changeAdminData', async (req, res) => {
  const { name, surname, username, newPassword } = req.body;
  const userId = globalUserId // assumption: User is authenticated and their ID is included in the request

  try {
    let queryParams = [];
    let fieldsToUpdate = [];

    // check which fields need to be updated and prepare accordingly
    if (name && name.length > 0) {
      fieldsToUpdate.push('name');
      queryParams.push(name);
    }
    if (surname && surname.length > 0) {
      fieldsToUpdate.push('surname');
      queryParams.push(surname);
    }
    if (newPassword && newPassword.length > 0) {
      const hashedPassword = await bcrypt.hash(newPassword, 10);
      fieldsToUpdate.push('password');
      queryParams.push(hashedPassword);
    }
    if (username && username.length > 0) {
      fieldsToUpdate.push('username');
      queryParams.push(username);
    }

    // check if there are fields to update
    if (fieldsToUpdate.length === 0) {
      return res.status(400).json({ success: false, error: "Es wurden keine Daten zum Aktualisieren übergeben.", errorColor: "warning" });
    }

    // update fields in the database
    let updateQuery = `UPDATE userdata SET role = 'admin'`;
    for (let i = 0; i < fieldsToUpdate.length; i++) {
      updateQuery += `, ${fieldsToUpdate[i]} = $${i + 1}`;
    }
    updateQuery += ` WHERE id = $${fieldsToUpdate.length + 1}`;
    queryParams.push(userId);

    try {
      console.log("Vor der Ausführung des UPDATE-Statements");
      await pool.query(updateQuery, queryParams);
      console.log("Nach der Ausführung des UPDATE-Statements: Erfolgreich");
    } catch (error) {
      console.error("Fehler beim Ausführen des UPDATE-Statements:", error);
      return res.status(500).json({ success: false, error: 'Interner Serverfehler' });
    }

    // return success message to the web server
    res.status(200).json({ success: true, message: 'Daten erfolgreich aktualisiert.' });

  } catch (error) {
    console.error('Fehler beim Aktualisieren der Benutzerdaten:', error);
    res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});

// get route of all registered users
idp.get('/auth/userdata', async (req, res) => {
  try {
    const result = await pool.query('SELECT * FROM userdata');
    const userData = result.rows;
    res.json(userData);
  } catch (error) {
    console.error('Fehler beim Abrufen der userdaten:', error.message);
    res.status(500).send('Interner Serverfehler beim IDP');
  }
});

// get route to send back current user id to webserver
idp.get('/auth/getUserId', (req, res) => {
  const username = globalUserName;
  const userId = globalUserId;
  console.log('in auth/getUserID', globalUserId);
  
  if (userId !== undefined && userId !== null) {
    console.log('userId (direct value):', userId);
    res.json({ user_id: userId,
              req_username: username,
     });
  } else {
    console.error('Benutzer nicht ordnungsgemäß authentifiziert.');
    res.status(401).json({ success: false, message: 'Benutzer nicht authentifiziert' });
  }
});

idp.get('/auth/getAdminById', async (req, res) => {
  const userId = globalUserId;

  try {
    if (userId != null) {
      const userData = await pool.query('SELECT * FROM userdata WHERE id = $1', [userId]);

      if (userData.rows.length > 0) {
        res.json(userData.rows[0]); 
      } else {
        res.json({ success: false, error: "Zur angemeldeten ID sind keine Daten vorhanden." });
      }
    } else {
      res.status(400).json({ success: false, error: "Ungültige Benutzer-ID." });
    }
  } catch (error) {
    console.error('Fehler beim Abrufen der Benutzerdaten:', error);
    res.status(500).json({ success: false, error: "Interner Serverfehler" });
  }
});

// post route to delete user
idp.post('/auth/deleteuser', async (req, res) => {
  const userIdToDelete = req.body.id;
  try {
    for (const userId of userIdToDelete) {
      const deleteQuery = 'DELETE FROM userdata WHERE id = $1';
      await pool.query(deleteQuery, [userId]);
    }
    res.status(204).send();
  } catch (error) {
    console.error('Fehler beim Löschen des Benutzers:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// post route to add user
idp.post('/auth/adduser', async (req, res) => {
  console.log('Request Body:', req.body);
  const { username, name, surname, email, password, role } = req.body;

  try {
    // check if user already exists
    const checkUserQuery = 'SELECT username, email FROM userdata WHERE username = $1 OR email = $2';
    const checkUserResult = await pool.query(checkUserQuery, [username, email]);

    if (checkUserResult.rows.length > 0) {
      const existingUser = checkUserResult.rows[0];

      let errorMessage;
      let errorColor;

      if (existingUser.username === username) {
        errorMessage = "Benutzername bereits vergeben.";
        errorColor = "warning";
      } else {
        errorMessage = "E-Mail-Adresse existiert bereits.";
        errorColor = "warning";
      }

      return res.status(400).json({ success: false, error: errorMessage, errorColor: errorColor });

    } else {
      const hashedPassword = await bcrypt.hash(password, 10);
      const registerQuery = 'INSERT INTO userdata (username, name, surname, email, password, role) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *';
      const newUser = await pool.query(registerQuery, [username, name, surname, email, hashedPassword, role]);

      res.status(201).json({ success: true, message: "Benutzer erfolgreich hinzugefügt", newUser: newUser.rows[0] });
    }

  } catch (error) {
    console.error('Fehler beim Hinzufügen des Benutzers nach IDP:', error);

    res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});

// post route to reload /admin page
idp.get('/admin/refresh', async (req, res) => {
  try {
    const query = 'SELECT * FROM userdata';
    const result = await pool.query(query);

    const data = result.rows;
    res.render('admin', { data });
  } catch (err) {
    console.error('Fehler beim Verarbeiten der Anfrage:', err);
    res.status(500).send('Interner Serverfehler');
  }
});

// in Kubernetes this is handled via Services
const port = process.env.PORT || 3001;

idp.get('/', (req, res) => {
  res.send('IDP Server läuft');
});

idp.listen(port, () => {
  console.log(`IDP is now listening on http://localhost:${port}`);
});

module.exports = idp;

  