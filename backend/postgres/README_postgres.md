# Datenbank direkt über den Postgres Pod anlegen
`kubectl get pods`<br>
`kubectl exec -it <pod> -- /bin/bash`<br>
`root@postgres-0:/# createdb -U postgres user_db`<br>
`psql -U postgres -d user_db`<br>
- Tabellen anlegen wie unten beschrieben
## Check database
- Zeigt eine Liste alle Datenbanken
`psql -U postgres -l`<br>
- Tabelle anzeigen
`\dt`<br>
- Shell verlassen
`exit`<br>

## Alternativ: Datenbank mittels pdadmin anlegen
`kubectl port-forward service/pgadmin-service 8000:80`<br>
Anmeldung<br>
- username: admin@admin.de // entsprechend dem Secret
- password: admin13 // entsprechend dem Secret
Anmeldung des Servers<br>
    - Server > Register
    - Connection:
        - host: postgres-service
        - port: 5432
        - maintenance database: postgres
        - username: posstgres // entsprechend dem Secret
        - Password: posstgres13 // entsprechend dem Secret

    - unter Databases per Rechtsklick Create > Database 
      erstellen der Datenbank <user_db>
    - user_db > Schemas > Tables > Query Tool
      hier werden die Tabellen erstellt

# Tabellen anlegen
## 1. Tabelle userdata anlegen

```
-- Create table userdata
-- Create the sequence first
CREATE SEQUENCE userdata_id_seq;

-- Create the table
CREATE TABLE IF NOT EXISTS public.userdata
(
    username character varying(200) COLLATE pg_catalog."default",
    name character varying(200) COLLATE pg_catalog."default",
    surname character varying(200) COLLATE pg_catalog."default",
    email character varying(200) COLLATE pg_catalog."default",
    password character varying(1000) COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('userdata_id_seq'::regclass),
    role character varying(50) COLLATE pg_catalog."default" DEFAULT 'USER',
    CONSTRAINT userdata_pkey PRIMARY KEY (id),
    CONSTRAINT username UNIQUE (username)
)
TABLESPACE pg_default;
```
## 2. Tabelle sessions anlegen

```
-- Table: public.sessions

-- DROP TABLE IF EXISTS public.sessions;

CREATE TABLE IF NOT EXISTS public.sessions
(
    sid character varying COLLATE pg_catalog."default" NOT NULL,
    sess json NOT NULL,
    expire timestamp(6) without time zone NOT NULL,
    user_id integer,
    CONSTRAINT sessions_pkey PRIMARY KEY (sid),
    CONSTRAINT fk_user_sessions FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT sessions_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

-- Index: idx_sessions_expire

-- DROP INDEX IF EXISTS public.idx_sessions_expire;

CREATE INDEX IF NOT EXISTS idx_sessions_expire
    ON public.sessions USING btree
    (expire ASC NULLS LAST)
    TABLESPACE pg_default;
```

## 3. Tabelle fotos anlegen
```
-- Table: public.fotos

-- DROP TABLE IF EXISTS public.fotos;

-- Erstelle die Sequenz
CREATE SEQUENCE fotos_id_seq;

-- Erstelle die Tabelle
CREATE TABLE IF NOT EXISTS public.fotos
(
    id integer NOT NULL DEFAULT nextval('fotos_id_seq'::regclass),
    image_path text COLLATE pg_catalog."default",
    title character varying(255) COLLATE pg_catalog."default",
    user_id integer,
    CONSTRAINT fotos_pkey PRIMARY KEY (id),
    CONSTRAINT fk_userdata FOREIGN KEY (user_id)
        REFERENCES public.userdata (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
);
```
