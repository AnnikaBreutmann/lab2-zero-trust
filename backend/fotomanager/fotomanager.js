const express = require('express');
const path = require('path');
const cors = require('cors');
const fotoConfig = require('./configs/configFotoDB');
const { Pool } = require('pg');
const pool = new Pool(fotoConfig);
const bodyParser = require('body-parser');

const fotomanager = express();
fotomanager.use(cors());
fotomanager.use(bodyParser.json());

const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, path.join(__dirname, 'public', 'images'));
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, Date.now() + path.extname(file.originalname))
    }   
})

const upload = multer({storage: storage});

// middleware for error handling
fotomanager.use((err, req, res, next) => {
    console.error('Error:', err);
    res.status(500).send('Internal Server Error');
    next(err);
});

// check for certain user id all fotos uploaded
fotomanager.get('/api/fotosUserId', async (req, res) => {
  try {
    const userId = req.query.userId;

    const allFotosUserId = await getFotosUserId(userId);
    res.status(200).json(allFotosUserId);
  } catch (error) {
    console.error('Error fetching fotos from database:', error.message);
    res.status(500).send('Internal Server Error');
  }
});

// get current user id to select all fotos uploaded
async function getFotosUserId(userId) {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM fotos WHERE user_id = $1', [userId]);

    if (result.rows.length > 0) {
      const fotoData = result.rows;
      client.release();
      return fotoData;
    } else {
      console.error('Keine Fotodaten enthalten für userId:', userId);
      return []; 
    }
  } catch (error) {
    console.error('Fehler beim Abrufen von Fotos aus der Datenbank:', error.message);
    throw error;
  }
}

// get handler gives webserver information back on path of images
fotomanager.get('/api/fotos', async (req, res) => {
  try {
      const client = await pool.connect();
      const result = await client.query('SELECT image_path, user_id FROM fotos');
      const data = {
          imagePaths: result.rows.map(row => row.image_path),
          user_ids: result.rows.map(row => row.user_id),
      };
      client.release();
      res.json(data);
  } catch (error) {
      console.error('Fehler beim Abrufen der Bildpfade aus der Datenbank:', error);
      res.status(500).send("Fehler beim Abrufen der Bildpfade aus der Datenbank.");
  }
});

// post handler to delete images
fotomanager.post('/api/delete_images', async (req, res) => {
  try {
    const { imagePath } = req.body;

    const fullPath = path.join(__dirname, 'public', imagePath);
    await fs.unlink(fullPath);

    const client = await fotoPool.connect();
    try {
      await client.query('DELETE FROM fotos WHERE image_path = $1', [imagePath]);
      res.status(200).send('Bild erfolgreich gelöscht');
      console.log('Bild erfolgreich gelöscht');
    } finally {
      client.release();
    }
  } catch (error) {
    console.error('Fehler beim Löschen des Bildes:', error);
    res.status(500).json({ success: false, message: 'Interner Serverfehler' });
  }
});

// post handler to delete singel fotos at admin_fotogalerie
fotomanager.post('/api/deletefotos', async (req, res) => {
  try {
    const fotosToDelete = req.body.uploaded_fotos;

    const placeholders = fotosToDelete.map((_, index) => `$${index + 1}`).join(', ');
    const deleteQuery = `DELETE FROM fotos WHERE image_path IN (${placeholders})`;
    await pool.query(deleteQuery, fotosToDelete);
    console.log('Fotos erfolgreich aus DB gelöscht:', fotosToDelete);

    res.status(200).send();
  } catch (error) {
    console.error('Fehler beim Löschen der Fotos:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// post hander to delete fotos for users
fotomanager.post('/api/deletefotosById', async (req, res) => {
  const client = await pool.connect();
  try {
    const userIds = req.body.user_ids; 

    if (userIds && userIds.length > 0) {

      for (const userId of userIds) {
        const checkQuery = await client.query('SELECT * FROM fotos WHERE user_id = $1', [userId]);

        if (checkQuery.rows.length > 0) {

          await client.query('DELETE FROM fotos WHERE user_id = $1', [userId]);
          console.log('Fotos erfolgreich für Benutzer gelöscht:', userId);
        } else {
          console.log('Benutzer hat bisher keine Fotos hochgeladen:', userId);
        }
      }

      res.status(200).send('Fotos erfolgreich gelöscht');
    } else {
      res.status(400).send('Keine Benutzer-IDs übergeben');
    }

  } catch (error) {
    console.error('Fehler beim Löschen der Fotos:', error);
    res.status(500).send('Interner Serverfehler');
  } finally {
    client.release();
  }
});

// post route: deletes a single image from the database based on the provided image path
fotomanager.post('/api/deleteSingleimage', async (req, res) => {
  let client = null; 

  try {
    client = await pool.connect();
    const imagePath  = req.body.imagePath;
    console.log('fotomanager:', [imagePath]);
    await client.query('DELETE FROM fotos WHERE image_path = $1', [imagePath]);
    res.sendStatus(200);
  } catch (error) {
    console.error('Fehler beim Löschen des Bildes:', error);
    res.status(500).json({ success: false, message: 'Interner Serverfehler' });

  }
});

// post route: uploads an image and inserts its data into the database.
fotomanager.post('/api/upload', upload.single('image'), async (req, res) => {

  const { userId, title, imagePath } = req.body;
  const user_id = userId.user_id;
  
  try {
    const client = await pool.connect();
    const query = 'INSERT INTO fotos (image_path, title, user_id) VALUES ($1, $2, $3) RETURNING *';
    const values = [imagePath, title, user_id];

    const result = await client.query(query, values);
    client.release();

    res.status(204).send('Bild erfolgreich hochgeladen.');
  } catch (error) {
    console.error('Fehler beim Einfügen des Eintrags in die Datenbank:', error);
    res.status(500).send("Fehler beim Hochladen des Bildes.");
  }
});

// get handler to start server
fotomanager.get("/", (req, res) => {
  res.send("Willkommen auf meinem Fotomanager-Server!");
});

const port = process.env.PORT || 3002;

fotomanager.listen(port, () => {
  console.log(`Fotomanager is now listening on http://localhost:${port}`);
});

module.exports = fotomanager;

  