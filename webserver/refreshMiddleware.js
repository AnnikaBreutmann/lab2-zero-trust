const express = require('express');
const router = express.Router();

// middleware to reload age
router.post('/admin/refresh', (req, res) => {
  res.redirect('/admin');
});

module.exports = router;