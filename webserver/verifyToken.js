const jwt = require('jsonwebtoken');

// function to verify token
function verifyToken(req, res, next) {
  // read token from header
  const token = req.headers['authorization'];

  // check if token is correct
  if (token) {
    jwt.verify(token, 'your_secret_key', (err, decoded) => {
      if (err) {
        return res.status(401).json({ success: false, error: 'Token ungültig' });
      } else {
        req.user = decoded; 
        next(); 
      }
    });
  } else {
    return res.status(403).json({ success: false, error: 'Token fehlt' });
  }
}

module.exports = verifyToken;
