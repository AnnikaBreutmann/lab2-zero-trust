const fs = require('fs');
const path = require('path');

// function to delte image file from local folder
function deleteImageFromFolder(imagePath) {
  const fullPath = path.join(__dirname, 'public', 'images', imagePath);

  fs.unlink(fullPath, (err) => {
    if (err) {
      console.error('Fehler beim Löschen des Bildes:', err);
    } else {
      console.log('Bild erfolgreich gelöscht:', fullPath);
    }
  });
}

module.exports = { deleteImageFromFolder };