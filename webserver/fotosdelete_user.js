// function to delte fotos
function updateDeleteFotos() {
  const checkedCheckbox = document.querySelector('input[name="fotoToDelete"]:checked');

  if (checkedCheckbox) {
      const image_path = checkedCheckbox.value;
      document.getElementById('deleteFotoId').value = image_path;
      console.log('Image Path des ausgewählten Fotos:', image_path);
  } else {
      document.getElementById('deleteFotoId').value = '';
      console.log('Kein Foto ausgewählt.');
  }
}x

function deleteFoto() {
  const checkedCheckboxes = document.querySelectorAll('input[name="fotoToDelete"]:checked');

  if (checkedCheckboxes.length > 0) {
    const markedFotoIds = Array.from(checkedCheckboxes).map(checkbox => checkbox.value);
    console.log('Marked Foto title:', markedFotoIds);

    const host = window.location.hostname; // hostname of current age
    //const port = window.location.port || "8001"; // not necessary in kubernetes

    // send api req to webserver
    fetch(`https://${host}/api/deletefotos`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ uploaded_fotos: markedFotoIds }),
    })
      .then(response => response.json())  
      .then(data => {
        console.log('Server-Antwort:', data); 
        if (data.success) {
          window.location.href = '/user';
          console.log('Antwort des Webservers ok');
        } else {
          console.error('Fehlerhafte Server-Antwort:', data);
        }
      })
      .catch(error => {
        console.error('Fehler beim Löschen der Fotos:', error);
      });
  } else {
    alert('Bitte mindestens ein Foto auswählen.');
  }
}

// reload current page
function refreshPage() {
  window.location.reload('/user');
}






