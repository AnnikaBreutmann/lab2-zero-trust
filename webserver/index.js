const express = require('express');
const cors = require('cors');
const axios = require('axios');
const session = require('express-session');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const refreshMiddleware = require('./refreshMiddleware');
const path = require('path');
const fs = require('fs').promises;
const { deleteImageFromFolder } = require('./imageUtils');

// initialize express server app
const app = express();

app.use(cors());
// middleware for refreshing
app.use(refreshMiddleware);
// serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public', 'images')));
app.use('/auth/styles', express.static(path.join(__dirname, 'styles')));
app.use(express.static(__dirname));

// set view engine to EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// parse json body
app.use(bodyParser.json());
//parse url encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

// customized middleware for logs in kubernetes to check req
app.use((req, res, next) => {
  console.log(`1. Received request for ${req.method} ${req.url}`);
  next();
});

// error handler
function renderError(res, error) {
  console.error('Fehler:', error);
  res.status(500).render('error', { error: error.message });
}

//---ROUTES FOR ALL USER---
// all routes accessible without login
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/impressum', (req, res) => {
  res.sendFile(path.join(__dirname, 'impressum.html'));
});

app.get('/fotogalerie', (req, res) => {
  res.render('fotogalerie'); 
});

app.get('/login_register', (req, res)=> {
  const errorDetails = req.query.error ? req.query.error : null;
  res.render('login_register', { error: errorDetails });
});

app.get('/login', (req, res) => {
  const errorDetails = req.query.error ? req.query.error : null;
  res.render('login', { error: errorDetails });
});

app.use(session({
  secret: 'geheimesgeheimnis',
  resave: false, 
  saveUninitialized: false
}));

//---ALL REGISTERED USERS---
// get routes for registered users with role user
app.get('/user_index', ensureAuthenticated, (req, res)=>{
  res.render('user_index');
});

app.get('/user_fotogalerie', ensureAuthenticated, (req, res) => {
  res.render('user_fotogalerie');
});

app.get('/user_impressum', ensureAuthenticated, (req, res) => {
  res.render('user_impressum');
});

app.get('/user_upload', ensureAuthenticated, (req, res) => {
  res.render('user_upload');
});

app.get('/admin_impressum', ensureAuthenticated, (req, res)=>{
  res.render("admin_impressum");
});

let userId;
let username;

// render user page and set 2FA-token in hidden field for OPA
async function renderUserPage(req, res, username, userId, fotosData, error, success) {
  const cookies = req.headers.cookie;
  let token;
  let new2FAToken = undefined;
  
  if (cookies) {
    cookies.split(';').forEach(cookie => {
      const [cookieName, cookieValue] = cookie.trim().split('=');
      if (cookieName === 'token') {
        token = cookieValue;
      }
    });
  }
  if (!token) {
    // Handle missing token (redirect to login, etc.)
    res.status(401).send('Token missing.');
    return; // Stop further processing
  }
  try {
    const decodeToken = jwt.verify(token, 'your_secret_key');
    new2FAToken = decodeToken.is2FAEnabled;
  
    res.render('user', { username, userId, fotosData, is2FATokenSet: new2FAToken, error, success });
  } catch (err) {
    console.error('Fehler beim Rendern der Benutzerseite:', err);
    res.status(500).render('error', { error: 'Internal Server Error render' });
  }
}

// get route for page /user
app.get('/user', ensureAuthenticated, async (req, res) => {
  try {
    const idpResponse = await fetch('http://idp-service:3001/auth/getUserId');
    
    if (!idpResponse.ok) {
      console.error('Fehler beim Abrufen der Benutzer-ID vom IDP:', idpResponse.statusText);
      throw new Error('Fehler beim Abrufen der Benutzer-ID vom IDP');
    }

    const idpData = await idpResponse.json();

    if (idpData != null) {
      userId = idpData.user_id;
      
      const fotomanagerResponse = await fetch(`http://fotomanager-service:3002/api/fotosUserId?userId=${userId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (fotomanagerResponse.ok) {
        const fotosData = await fotomanagerResponse.json();
        username = idpData.req_username;
        const error = req.query.error || null;
        const success = req.query.success || null;

        if (newUsername != null) {
          username = newUsername;
        }
        await renderUserPage(req, res, username, userId, fotosData, error, success);
      } else {
        console.error('Fehler beim Abrufen der Fotos vom Fotomanager:', fotomanagerResponse.statusText, fotomanagerResponse.status);
        res.status(500).render('error', { error: 'Fehler beim Abrufen der Fotos vom Fotomanager' });
      }
    } else {
      res.status(500).render('error', { error: 'Benutzer-ID nicht verfügbar' });
    }
  } catch (error) {
    console.error('Fehler beim Laden der Benutzerdaten:', error);
    res.status(500).render('error', { error: 'Internal Server Error' });
  }
});

// change user data 
app.post('/user', ensureAuthenticated, async (req, res) => {
  const changeUserData = req.body;
  const is2FATokenSet = req.body.is2FATokenSet;

  try {
    const idpResponse = await fetch('http://idp-service:3001/changeUserData', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(changeUserData, is2FATokenSet),
    });

    if (idpResponse.ok) {
      res.redirect('user');
    } else {
      const errorText = await idpResponse.text();
      res.status(idpResponse.status).send(errorText);
    }
  } catch (error) {
    res.status(500).send('Interner Serverfehler /user');
  }
});

// function to get user data from idp
async function getUserData(userId) {
  try {
    const idpUsernameResponse = await fetch(`http://idp-service:3001/auth/getUserName?userId=${userId}`);
    
    if (!idpUsernameResponse.ok) {
      console.error('Fehler beim Abrufen des Benutzernamens vom IDP:', idpUsernameResponse.statusText);
      throw new Error('Fehler beim Abrufen des Benutzernamens vom IDP');
    }

    const usernameData = await idpUsernameResponse.json();
    const username = usernameData.username; 

  } catch (error) {
    console.error('Fehler beim Laden der Benutzerdaten:', error);
    throw error;
  }
}

//---ALL REGISTERED ADMINS---
// get rout for /admin page
app.get('/admin', ensureAuthenticated, async (req, res) => {
  console.log('was ist in dem req.cookie', req.headers.cookie)
  // get jwt to set in hidden field is2FATokenSet
  const cookies = req.headers.cookie;
  let token;

  if (cookies) {
    cookies.split(';').forEach(cookie => {
      const [cookieName, cookieValue] = cookie.trim().split('=');
      if (cookieName === 'token') {
        token = cookieValue;
      }
    });
  }
  if (!token) {
    // Handle missing token (redirect to login, etc.)
    res.status(401).send('Unauthorized');
    return; // Stop further processing
  }
  try {
    // check 2fa status of cookie
    const decodeToken = jwt.verify(token, 'your_secret_key')
    const is2FATokenSet = decodeToken.is2FAEnabled;
    const userId = decodeToken.is2FAEnabled;
    console.log('token inhalt ', decodeToken)
    console.log('Decoded token:', decodeToken);
    // Benutzerdaten abrufen
    const userDataResponse = await fetch('http://idp-service:3001/auth/userdata');
    if (!userDataResponse.ok) {
      throw new Error('Failed to fetch user data from backend');
    }
    const userData = await userDataResponse.json();

    // Adminseite mit Benutzerdaten und 2FA-Status rendern
    res.render('admin', { userData, is2FATokenSet });
  } catch (error) {
    console.error('Error rendering admin page:', error.message);
    res.status(500).send('Internal Server Error /admin');
  }
});

// error handler for admin section
app.get('/admin/errorMessage', async (req, res) => {
  try {
    const userDataResponse = await fetch('http://idp-service:3001/auth/userdata');

    if (!userDataResponse.ok) {
      throw new Error('Failed to fetch user data from backend');
    }
    
    const userData = await userDataResponse.json();
    
    res.locals.addError = true;
    res.render('admin', { userData });
  } catch (error) {
    console.error('Error fetching data from backend:', error.message);
    res.status(500).send('Internal Server Error START');
  }
});
// reload /admin page
app.get('/admin/refresh', async (req, res) => {
  const cookies = req.headers.cookie;
  let token;

  if (cookies) {
    cookies.split(';').forEach(cookie => {
      const [cookieName, cookieValue] = cookie.trim().split('=');
      if (cookieName === 'token') {
        token = cookieValue;
      }
    });
  }
  if (!token) {
    // Handle missing token (redirect to login, etc.)
    res.status(401).send('Unauthorized');
    return; // Stop further processing
  }
  try {
    // check jwt token if is2FATokenSet is true or false
    const decodedToken = jwt.verify(token, 'your_secret_key');
    const is2FATokenSet = decodedToken.is2FAEnabled;

    // check user data 
    const userDataResponse = await fetch('http://idp-service:3001/auth/userdata');
    if (!userDataResponse.ok) {
      throw new Error('Failed to fetch user data from backend');
    }
    const userData = await userDataResponse.json();

    // render /admin with is2FATokenSet in hidden field to sent it later in req for OPA
    res.render('admin', { userData, is2FATokenSet });
  } catch (err) {
    console.error('Fehler beim Verarbeiten der Anfrage:', err);
    res.status(500).send('Interner Serverfehler /admin/refresh');
  }
});

// get route for /admin_start page
app.get('/admin_start', ensureAuthenticated, (req, res)=>{
  res.render('admin_start');
});

// get route for /admin_fotogalerie page
app.get('/admin_fotogalerie', ensureAuthenticated, (req, res) => {
  res.render('admin_fotogalerie'); 
});

// // get route for /admin_settings to change admin data or delelet user
app.get('/admin_settings', ensureAuthenticated, async (req, res) => {
  try {
    const idpResponse = await fetch('http://idp-service:3001/auth/getAdminById');
    
    if (idpResponse.ok) {
      const user = await idpResponse.json();
      res.render('admin_settings', { user });

    } else {
      throw new Error('Fehler beim Abrufen der Benutzerdaten vom IDP');
    }
  } catch (error) {
    console.error('Fehler beim Abrufen der Benutzerdaten:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// delete fotos
app.post('/api/deletefotos', async (req, res) => {
  try {

    const fotosToDelete = req.body.uploaded_fotos;
    let successFolderDelete = true; 

    if (fotosToDelete && fotosToDelete.length > 0) {

      for (const imagePath of fotosToDelete) {

        const imagePathWithoutPrefix = imagePath.replace('/images/', '');
        deleteImageFromFolder(imagePathWithoutPrefix);
        console.log('Foto erfolgreich aus Ordner gelöscht:', imagePathWithoutPrefix);
        successFolderDelete = successFolderDelete && true; 
      }

      const fotomanagerResponse = await fetch('http://fotomanager-service:3002/api/deletefotos', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ uploaded_fotos: fotosToDelete }),
      });

      if (successFolderDelete && fotomanagerResponse.ok) {
        res.send({ success: true });
      } else {
        res.redirect('/user?error=Fehler beim Löschen der Fotos aus der Fotodatenbank.');
      }

    } else {
      console.error('Keine Fotos zum Löschen mehr vorhanden.');
      res.render('user', { error: { details: 'Keine Fotos zum Löschen ausgewählt.', color: 'red' } });
    }
  } catch (err) {
    console.error('Fehler beim Verarbeiten der Anfrage:', err);
    res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});

app.post('/auth/login', async (req, res) => {
  try {
    const { usernameOrEmail, password } = req.body;

    // send login data to IDP
    const response = await fetch('http://idp-service:3001/auth/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ usernameOrEmail, password }),
    });

    // check for successful HTTP status code
    if (response.ok) {
      // parse response as JSON
      const responseJson = await response.json();

      // handle successful login
      if (responseJson.success) {
        let token;
        if (responseJson.hasOwnProperty('token')) {
          token = responseJson.token;
        if (responseJson.hasOwnProperty('user')) {
          req.session.user = responseJson.user;
          console.log('/login ', req.session.user);
        }
        } else {
          // handle missing token
          console.error('IDP response missing token');
          return res.render('login_register', { error: { details: 'Login erfolgreich, aber Abruf des Tokens fehlgeschlagen.', color: 'red' } });
        }

        // set the HTTP-Only cookie securely
        await res.cookie('token', token, { httpOnly: true, secure: true });

        try {
          // decode the JWT token to extract payload (optional)
          const decodedToken = jwt.verify(token, 'your_secret_key');
          const role = decodedToken.role;

          if (role === 'admin') {
            res.redirect('/admin');
          } else if (role === 'user') {
            res.redirect('/user_upload');
          } else {
            res.render('login_register', { error: { details: 'Unbekannte Benutzerrolle.', color: 'red' } });
          }
        } catch (error) {
          console.error('Error decoding token:', error.message);
          // handle potential invalid token or unexpected format
          return res.status(401).render('login', { error: { details: 'Fehlerhafte Anmeldedaten.', color: 'red' } });
        }
      } else {
        // handle failed login based on IDP response
        if (responseJson.error === 'unauthorized') {
          res.status(401).render('login', { error: { details: 'Falsche Passworteingabe oder Benutzername.', color: 'red' } });
        } else {
          res.status(500).send('Internal Server Error (Missing error message from IDP)');
        }
      }
    } else {
      res.render('login_register', { error: { details: 'Falsche Passworteingabe oder Benutzername.', color: 'red' } });
    }
  } catch (error) {
    console.error('Error handling login request:', error.message);
    console.error('Error stack:', error.stack);
    res.status(500).send('Internal Server Error (Error sending login request)');
  }
});

// get route to get 2fa token
app.get('/login_register/2fa', (req, res)=> {
  const errorDetails = req.query.error ? req.query.error : null;
  res.render('login_register_2fa', { error: errorDetails });
});

let temporaryTOTP;
// get route to desiplay 2fa token
app.get('/auth/2fa/simple', async (req, res) => {

  try {
      const idpResponse = await fetch('http://idp-service:3001/auth/2fa/simple');

      if (!idpResponse.ok) {
        console.error('Fehler beim Generieren des TOTP-Geheimcodes:', idpResponse.statusText);
        res.status(500).json({ error: 'Interner Serverfehler beim Abrufen des TOTP-Codes vom IDP' });
        return;
      } 

      const idpData = await idpResponse.json();
      temporaryTOTP = idpData.temporaryTOTP;
      console.log('Token temporaryTOTP ' + temporaryTOTP);
      res.render('twofactor', { totpSecret: temporaryTOTP });
  } catch (error) {
      console.error('Fehler beim Generieren des TOTP-Geheimcodes:', error);
      res.status(500).json({ error: 'Interner Serverfehler' });
  }
});

// login für user with 2fa token set
app.post('/auth/login/2fa_simple', async (req, res) => {
  try {
    const { usernameOrEmail, password, totpCode } = req.body;
    console.log('/auth/login/2fa_simple ' + totpCode)

    // send login data to IDP
    const response = await fetch('http://idp-service:3001/auth/login/2fa', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ usernameOrEmail, password, totpCode }),
    });

    // check for successful HTTP status code
    if (response.ok) {
      // parse response as JSON
      const responseJson = await response.json();

      // handle successful login
      if (responseJson.success) {
        let token;
        if (responseJson.hasOwnProperty('token')) {
          token = responseJson.token;
          if (responseJson.hasOwnProperty('user')) {
            req.session.user = responseJson.user;
            console.log('/login ', req.session.user);
          }
        } else {
          return res.render('login_register', { error: { details: 'Login erfolgreich, aber Abruf des Tokens fehlgeschlagen.', color: 'red' } });
        }
        await res.cookie('token', token, { httpOnly: true, secure: true });

        try {
          const decodedToken = jwt.verify(token, 'your_secret_key');
          const role = decodedToken.role;
          if (role === 'admin') {
            res.redirect('/admin');
          } else if (role === 'user') {
            res.redirect('/user_upload');
          } else {
            res.render('login_register', { error: { details: 'Unbekannte Benutzerrolle.', color: 'red' } });
          }
        } catch (error) {
          console.error('Error decoding token:', error.message);
          return res.status(401).render('login', { error: { details: 'Fehlerhafte Anmeldedaten.', color: 'red' } });
        }
      } else {
        if (responseJson.error === 'unauthorized') {
          res.status(401).render('login', { error: { details: 'Falsche Passworteingabe oder Benutzername.', color: 'red' } });
        } else {
          res.status(500).send('Internal Server Error (Missing error message from IDP)');
        }
      }
    } else {
      res.render('login_register', { error: { details: 'Falsche Passworteingabe oder Benutzername.', color: 'red' } });
    }
  } catch (error) {
    console.error('Error handling login request:', error.message);
    console.error('Error stack:', error.stack);
    res.status(500).send('Internal Server Error (Error sending login request)');
  }
});

// check if user is authenticated
function ensureAuthenticated(req, res, next) {
  if (req.session && req.session.user) {
    next();
  } else {
    res.status(401).send('Zugriff verweigert. Bitte zuerst anmelden.');
  }
}

// post to register user
app.post('/auth/register', async (req, res) => {
  try {
    const { usernameRegister, nameLogIn, surnameLogIn, email, passwordRegister } = req.body;

    const response = await fetch('http://idp-service:3001/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ usernameRegister, nameLogIn, surnameLogIn, email, passwordRegister }),
    });

    if (!response.ok) {
      res.status(response.status).send('Internal Server Error (Error from IDP)');
      return;
    }

    const responseJson = await response.json();

    if (!responseJson || !responseJson.hasOwnProperty('success')) {
      console.error('Invalid IDP response:', responseJson);
      res.status(500).send('Internal Server Error (Invalid IDP response)');
      return;
    }

    if (responseJson.success) {
      res.redirect('/login');
    } else {
      if (!responseJson.success) {
        res.render('login_register', { error: { details: responseJson.error, color: 'red' } });
      } else {
        res.status(500).send('Internal Server Error (Missing error message from IDP)');
      }
    }
  } catch (error) {
    console.error('Error handling registration request:', error.message);
    console.error('Error stack:', error.stack);
    res.status(500).send('Internal Server Error (Error sending registration request)');
  }
});

// logout and delete current session and jwt
app.post('/auth/logout', async (req, res) => {
  try {
    const idpLogoutResponse = await axios.post('http://idp-service:3001/auth/logout');

    if (idpLogoutResponse.status === 200) {
      req.session.destroy((error) => {
        if (error) {
          console.error('Fehler beim Session-Zerstören:', error);
          return next(error);
        }
        console.log('Benutzer ausgeloggt', req.user);
      });      res.cookie('token', '', { expires: new Date(0), httpOnly: true });
      res.redirect('/');
    }
  } catch (error) {
    console.error('Fehler beim Logout:', error.message);
    res.status(500).send('Interner Serverfehler beim Logout');
  }
});

// admins can change their userdata
app.post('/auth/changeadmin_settings', async (req, res) => {
  // Hier können Änderungen auftreten
  const changeData = req.body;
  try {
    const idpResponse = await fetch('http://idp-service:3001/auth/changeAdminData', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(changeData),
    });

    if (idpResponse.ok) {
      res.redirect('/admin_settings');
    } else {
      const errorText = await idpResponse.text(); 
      res.status(idpResponse.status).send(errorText);
    }
  } catch (error) {
    console.error('Fehler beim Ändern der Benutzerdaten:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// admins can add user
app.post('/auth/adduser', async (req, res) => {
  // retrieve token from cookies
  const cookies = req.headers.cookie;
  let token;

  if (cookies) {
    cookies.split(';').forEach(cookie => {
      const [cookieName, cookieValue] = cookie.trim().split('=');
      if (cookieName === 'token') {
        token = cookieValue;
      }
    });
  }

  // handle missing token
  if (!token) {
    res.status(401).send('Unauthorized'); // Redirect to login or show error
    return; // Stop further processing
  }

  try {
    // validate token (assuming JWT)
    const decodedToken = jwt.verify(token, 'your_secret_key');
    const is2FATokenSet = decodedToken.is2FAEnabled; // Assuming 'is2FAEnabled' property in token

    // extract user data from request body
    const { username, name, surname, email, role, password } = req.body;

    // create user object with validated token data
    const userToAdd = {
      username,
      name,
      surname,
      email,
      role,
      password,
      is2FATokenSet,
    };

    // send user data to backend server for addition
    const addUserResponse = await fetch('http://idp-service:3001/auth/adduser', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userToAdd),
    });

    if (addUserResponse.ok) {
      // user added successfully
      res.redirect('/admin/refresh');
    } else {
      throw new Error('Fehler beim Hinzufügen des Benutzers');
    }
  } catch (error) {
    console.error('Fehler beim Hinzufügen des Benutzers:', error);
    res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});


// delete user and fotos
app.post('/auth/deleteuserandfotos', async (req, res) => {
  const userIdToDelete = req.body.id;
  console.log('id', userIdToDelete);

  try {
    const fotoDeleteResponse = await fetch('http://fotomanager-service:3002/api/deletefotosById', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ user_id: userIdToDelete }),
    });

    const idpDeleteResponse = await fetch('http://idp-service:3001/auth/deleteuser', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: userIdToDelete }),
    });

    if (idpDeleteResponse.ok && fotoDeleteResponse.ok) {
      console.log('Benutzer und Fotos erfolgreich gelöscht:', userIdToDelete);
      res.redirect('/admin/refresh'); 
    } if (idpDeleteResponse.ok) {
      res.redirect('/admin/refresh'); 
    } else {
      console.log('antwort des idp ', idpDeleteResponse);
      const errorText = await idpDeleteResponse.text();
      console.error('Fehler beim Löschen des Benutzers:', errorText);
      res.status(idpDeleteResponse.status).send(errorText);
    }

  } catch (error) {
    console.error('Fehler beim Löschen des Benutzers und der Fotos:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// delete single foto from /admin_fotogalerie
app.post('/api/deleteSingleimage', async (req, res) => {
  try {
    const singleFotoToDelete = req.body;

    if (singleFotoToDelete && singleFotoToDelete.imagePath) {
      const fullPath = path.join(__dirname, 'public', singleFotoToDelete.imagePath);
      
      try {
        await fs.unlink(fullPath);
        console.log('Foto erfolgreich aus Ordner gelöscht:', singleFotoToDelete.imagePath);
      } catch (unlinkError) {
        console.error('Fehler beim Löschen des Bildes aus dem Ordner:', unlinkError);
        return res.status(500).json({ success: false, error: 'Fehler beim Löschen des Bildes aus dem Ordner' });
      }

      const fotomanagerResponse = await fetch('http://fotomanager-service:3002/api/deleteSingleimage', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(singleFotoToDelete),
      });

      if (fotomanagerResponse.ok) {
        res.json({ success: true, redirect: 'admin_fotogalerie' });
      } else {
        console.error('Fehler beim Löschen des Bildes in Fotomanager:', fotomanagerResponse.statusText);
        res.status(500).json({ success: false, error: 'Fehler beim Löschen des Bildes in Fotomanager' });
      }
    } else {
      console.error('Ungültige Anfrage: Keine Bildinformationen vorhanden.');
      res.status(400).json({ success: false, error: 'Ungültige Anfrage: Keine Bildinformationen vorhanden.' });
    }
  } catch (err) {
    console.error('Fehler beim Verarbeiten der Anfrage:', err);
    res.status(500).json({ success: false, error: 'Interner Serverfehler' });
  }
});

const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.join(__dirname, 'public', 'images'));

    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, Date.now() + path.extname(file.originalname))
    }   
})

const upload = multer({storage: storage});

// upload fotos and save path in database via fotomanager
app.post('/api/upload', upload.single('image'), async (req, res) => {
  try {
    // get id of current user
    const idpResponse = await fetch('http://idp-service:3001/auth/getUserId');

    if (!idpResponse.ok) {
      console.error('Fehler beim Abrufen der Benutzer-ID vom IDP:', idpResponse.statusText);
      throw new Error('Fehler beim Abrufen der Benutzer-ID vom IDP');
    }

    const idpData = await idpResponse.json();
    console.log('IDP Response:', idpData);

    if (idpData != null) {
      const userId = idpData;
      const title = req.body.title;
      const imagePath = path.join('/images', req.file.filename);

      const fotomanagerResponse = await fetch('http://fotomanager-service:3002/api/upload', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userId: userId,
          title: title,
          imagePath: imagePath
        }),
      });
      if (fotomanagerResponse.ok) {
        res.locals.uploadSuccess = true;
        res.render('user_upload');
      } else {
        res.status(500).send('Fotomanager hat den Upload nicht erfolgreich abgeschlossen.');
      }
    } else {
      console.error('IDP Response enthält keine Benutzer-ID oder ist ungültig:', idpData);
      res.status(500).send('Interner Serverfehler');
    }
  } catch (error) {
    console.error('Fehler beim Bild-Upload:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

let newUsername = null;
// change user data
app.post('/auth/changeUserData', async (req, res) => {
  const changeData = req.body;
  const changeUsername = req.body.username;
  
  try {
    const idpResponse = await fetch('http://idp-service:3001/auth/changeUserData', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(changeData),
    });

    if (idpResponse.ok) {
      newUsername = changeUsername;
      res.redirect('/user');
    } else {
      const errorText = await idpResponse.text(); 
      res.status(idpResponse.status).send(errorText);
    }
  } catch (error) {
    console.error('Fehler beim Ändern der Benutzerdaten:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// get all fotos from database and show them in fotogaleries
app.get('/api/fotos', async (req, res) => {
  try {
    const fotoManagerResponse = await fetch('http://fotomanager-service:3002/api/fotos');
    
    if (fotoManagerResponse.ok) {
      const fotoManagerData = await fotoManagerResponse.json();
      console.log('Benutzerinformationen:', req.user);
      console.log('Ausgabe webserver', fotoManagerData);
      res.send(fotoManagerData);
    } else {
      res.status(fotoManagerResponse.status).send('Fehler beim Abrufen der Bildinformationen');
    }
  } catch (error) {
    console.error('Fehler beim Abrufen der Bildinformationen:', error);
    res.status(500).send('Interner Serverfehler');
  }
});

// show /user for current user
app.post('/user', ensureAuthenticated, async (req, res) => {
  const changeUserData = req.body;
  const is2FATokenSet = req.body.is2FATokenSet;

  try {
    const idpResponse = await fetch('http://idp-service:3001/changeUserData', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(changeUserData, is2FATokenSet),
    });

    if (idpResponse.ok) {
      res.redirect('user');
    } else {
      const errorText = await idpResponse.text();
      res.status(idpResponse.status).send(errorText);
    }
  } catch (error) {
    res.status(500).send('Interner Serverfehler /user');
  }
});

// function to update after delte user
function updateDeleteUserId() {
  const selectedUsers = getSelectedUsers();

  if (selectedUsers.length > 0) {
    const userId = selectedUsers[0];
    document.getElementById('userToDelete').value = userId;
  } else {
    document.getElementById('userToDelete').value = '';
  }
  updateSelectedUsersDisplay();
}

function updateSelectedUsersDisplay() {
  const selectedUsersContainer = document.getElementById('selectedUsers');
  const selectedUsers = getSelectedUsers();

  selectedUsersContainer.textContent = `Ausgewählte Benutzer: ${selectedUsers.join(', ')}`;
}

function getSelectedUsers() {
  const checkedCheckboxes = document.querySelectorAll('input[name="userToDelete"]:checked');
  const userToDelete = Array.from(checkedCheckboxes).map(checkbox => checkbox.value);

  return userToDelete;
}

// not necessary in kubernetes
const port = process.env.PORT || 8001;

app.listen(port, () => {
  console.log(`Start Webserver auf http://localhost:${port}`);
});
