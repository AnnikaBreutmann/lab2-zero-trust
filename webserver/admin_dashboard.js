// select the user to be deleted and display which user(s) is/are to be deleted
function updateDeleteUserId() {
  const selectedUsers = getSelectedUsers();

  if (selectedUsers.length > 0) {
    const userId = selectedUsers[0];
    document.getElementById('userToDelete').value = userId;
    console.log('ID des ausgewählten Benutzers:', userId);
  } else {
    document.getElementById('userToDelete').value = '';
    console.log('Kein Benutzer ausgewählt.');
  }
  updateSelectedUsersDisplay();
}

let selectedUsersContainer; 
function updateSelectedUsersDisplay() {
  selectedUsersContainer = document.getElementById('selectedUsers');
  const selectedUsers = getSelectedUsers();

  selectedUsersContainer.textContent = `Ausgewählte Benutzer: ${selectedUsers.join(', ')}`;
}

function getSelectedUsers() {
  const checkedCheckboxes = document.querySelectorAll('input[name="userToDelete"]:checked');
  const userToDelete = Array.from(checkedCheckboxes).map(checkbox => checkbox.value);

  return userToDelete.length > 0 ? userToDelete : [];
}

// delete selected user with his fotos
function deleteUserAndFotos() {
  const markedUserId = getSelectedUsers();

  if (markedUserId.length > 0) {
    const host = window.location.hostname; // hostname of current page
    //const port = window.location.port || "8001"; // not necessary in kubernetes
    fetch(`https://${host}/auth/deleteuserandfotos`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: markedUserId }),
    })
    .then(response => {
      if (response.ok) {
        return response.text();
      } else {
        throw new Error(`Fehler beim Löschen des Benutzers und der Fotos: ${response.statusText}`);
      }
    })
    .then(responseText => {
      console.log(responseText);
      window.location.href = '/admin/refresh'; 
    })
    .catch(error => {
      console.error('Fehler beim Löschen des Benutzers und der Fotos:', error.message);
      alert('Es ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Fehlerdetails finden Sie in der Konsole.');
      console.error(error); 
    });
    
  } else {
    alert('Bitte einen Benutzer auswählen.');
  }
}

// delete all data of fotos of user
function deleteFotos(userId) {
  fetch('https://fotomanager-service/api/deletefotos', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ user_id: userId }),
  })
  .then(data => {
    console.log('Fotos gelöscht für Benutzer:', userId);
  })
  .catch(error => {
    console.error('Fehler beim Löschen der Fotos:', error);
  });
}

// canceling and resetting the selection
function cancelDeletion(selectedUsersContainer) {
  const checkboxes = document.querySelectorAll('input[name="userToDelete"]');

  checkboxes.forEach(checkbox => {
    checkbox.checked = false;
  });

  document.getElementById('deleteUserId').value = '';
  getSelectedUsers(null);
  updateSelectedUsersDisplay();
  
  console.log('Löschung abgebrochen');
  window.location.href = '/admin';
}

