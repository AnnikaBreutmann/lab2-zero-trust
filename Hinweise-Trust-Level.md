Die Anwendung kann nun getestet werden. Folgende Trust Level bestehen:
- Die Anwendung ist nur über folgende Browser erreichbar: Firefox, Google Chrome, Safari*
*Die Auswahl erfolgte zufällig.

*Neue Funktionen, die sich durch das jeweilige Trust Level ergeben.*

# Trust Level für Admins
## Trust Level 0
Voraussetzung: **If role = admin & AdminAuth & Firefox & Chrome**
/admin	Nur Ansicht
/admin_settings	*Admins können ihre eigenen Daten ändern (wie Benutzername, Passwort)*
/admin_fotogalerie	Nur Ansicht

## Trust Level 1
Voraussetzung: **If role = admin & AdminAuth & Firefox**
/admin	*User können hinzugefügt werden nur mit Rolle user*
/admin_settings	Admins können ihre eigenen Daten ändern (wie Benutzername, Passwort)
/admin_fotogalerie	Nur Ansicht

## Trust Level 2
Voraussetzung: **If role = admin & AdminAuth & Firefox & 2FA**
/auth/adduser und /auth/deleteuserandfotos
*User können hinzugefügt werden mit Rolle user und admin und user können gelöscht werden, damit werden auch alle Fotos des Users gelöscht*
/admin_settings	Eigene Daten ändern können
/admin_fotogalerie	Bilder löschen können

# Trust Level für User
## Trust Level 0
Voraussetzung: **If role = user & Authenticated & Firefox & Chrome**
/upload	*Bilder hochladen*
/fotogalerie Nur Ansicht
/user Nur Ansicht
## Trust Level 1
Voraussetzung: **If role = user & Authenticated & Firefox**
/upload	Bilder hochladen
/fotogalerie	Nur Ansicht
/user *Bilder löschen können*

## Trust Level 2
Voraussetzung: **If role = user & Authenticated & Firefox & & 2FA**
/upload	Bilder hochladen
/fotogalerie Nur Ansicht
/user *Einstellungen ändern können*




